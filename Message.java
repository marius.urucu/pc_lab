/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.structs;

/**
 *
 * @author student
 */
public class Message {

    private String mName;
    private String mContent;

    @Override
    public String toString() {
        return mName + ":" + mContent;
    }

    public Message(String name, String content) {
        mName = name;
        mContent = content;

    }

}
