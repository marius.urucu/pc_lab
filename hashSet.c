#include "LinkedList.h"
#include "person.h"
#include <stdio.h>
#include <stdlib.h>
#include "hashset.h"




struct HashSet * createHashSet(){
    struct HashSet * newHash = (struct HashSet *) calloc(1, sizeof(struct HashSet));
    newHash->size=0;
    return newHash;
}
 

unsigned hashSetSize(struct HashSet * set){
    return set->size;
}
 

char hashSetIsEmpty(struct HashSet * set){
    if(set->size==0){
        return 1;
    }
    else return 0;
}
 

void hashSetPut(struct HashSet * set, struct Person person){
    unsigned i,j;
    i=hash(person);
    j=linkedListSearch(&set->array[i],person);
    if(j==-1){
        linkedListInsert(&set->array[i], i, person);
        set->size++;
    }
    
    
    
}
 

void hashSetRemove(struct HashSet * set, struct Person person){
    int i,j;
    i=hash(person);
    j=linkedListSearch(&set->array[i],person);
    if(j!=-1){
        linkedListDelete(&set->array[i], i);
        set->size--;
    }
    
}
 

char hashSetContains(struct HashSet * set, struct Person person){
    int i,j;
    i=hash(person);
    j=linkedListSearch(&set->array[i],person);
    if(j!=-1){
        return 1;
    }
    else return 0;
}

 

void deleteHashSet(struct HashSet * set){
    int i;
    for(i=0;i<=999999;i++){
        while(linkedListSize(&set->array[i]))
        linkedListDelete(&set->array[i],0 );
    }
    free(set);
}