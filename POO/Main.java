
 package seriaf.poo.structs;
 
 
 public class Main {
 
     public static void main(String[] args) {
         PrivateMessage m1 = new PrivateMessage("Marius", "Vali", "Buna ziua!");
         PrivateMessage m2 = new PrivateMessage("Marius", "Cristiana", "Hey!Ce faceti?");
         PrivateMessage m3 = new PrivateMessage("Marius", "Dan", "Noi bine,tu?!");
 
         System.out.printf("%s %s\n", m1.toString(), m1.getRecipient());
         System.out.printf("%s %s\n", m2.toString(), m2.getRecipient());
         System.out.printf("%s %s\n", m3.toString(), m3.getRecipient());
 
     }
 
 }
