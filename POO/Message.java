
package seriaf.poo.structs;

public class Message {
    private String mName;
    private String mContent;
    @Override
    public String toString() {
        return mName + ":" + mContent;
    }
    public Message(String name, String content) {
        mName = name;
        mContent = content;
    }
}