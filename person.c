#include <string.h>

#include "person.h"
#include <stdio.h>
#include <stdlib.h>


char equals(struct Person person1, struct Person person2){
    if(strcmp(person1.cnp,person2.cnp)==0){
        return 1;
    }
    else return 0;
}

unsigned hash(struct Person person){
    int i;
    i = atoi (person.cnp+7);
    return i;
}

