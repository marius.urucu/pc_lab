
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "hashset.h"
#include "person.h"

struct Person readFromKeyboard(){
    struct Person person;
    fgets (person.firstName, 30, stdin);
    
    fgets (person.lastName, 30, stdin);
    
    fgets (person.idNumber, 9, stdin);
    
    fgets (person.address, 255, stdin);
    
    fgets (person.birthday, 11, stdin);
    
    fgets (person.cnp, 14, stdin);
    
    return person;
}

int main() {
    struct Person person;
    struct HashSet * set;
    set=createHashSet();
    do{
       person=readFromKeyboard();
       hashSetPut(set, person);
       
    }while(!strcmp(person.firstName,"-")==0);
    
    printf("%d persoane unice",set->size);
    
    return (EXIT_SUCCESS);
}

